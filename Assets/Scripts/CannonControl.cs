using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonControl : MonoBehaviour {

	public float RotateSpeed;
	public Transform[] BallPrefab;
	public Transform SpawnPoint;
	public float ShootForce;
	//public Material[] mat;
	public Renderer CanonRenderer;
    public Transform NextBallPosition;
    Transform NextBall;

	// Use this for initialization
	void Start () {
		
		ChangeColor ();
	}
	
	// Update is called once per frame
	void Update () {

		if(Input.GetKey(KeyCode.RightArrow)==true){

			transform.Rotate (new Vector3 (0,0,-RotateSpeed));

		}
		if(Input.GetKey(KeyCode.LeftArrow)==true){

			transform.Rotate (new Vector3 (0,0,RotateSpeed));

		}
		if (Input.GetKeyDown (KeyCode.Space) == true) {

			Transform NewBall = Instantiate (NextBall);
			NewBall.position = SpawnPoint.position;
			NewBall.rotation = SpawnPoint.rotation;
			

            Vector3 DirectionVector = SpawnPoint.position - transform.position;
            DirectionVector = DirectionVector * ShootForce;
            NewBall.GetComponent<Rigidbody2D>().simulated = true;
            NewBall.GetComponent<Rigidbody2D>().AddForce(DirectionVector);
            NewBall.GetComponent<Rigidbody2D>().AddTorque(1);

            ChangeColor();
            //GetComponent<AudioSource>().Play();
		}


	
	}	

	void ChangeColor(){

		int chosenMaterial = Random.Range (0,BallPrefab.Length);
        if (NextBall != null)
        {
            Destroy(NextBall.gameObject);
        }
        NextBall = Instantiate(BallPrefab[chosenMaterial], NextBallPosition.position, transform.rotation);
        NextBall.GetComponent<Rigidbody2D>().simulated = false;
		//CanonRenderer.material = mat [chosenMaterial];
	}

}
